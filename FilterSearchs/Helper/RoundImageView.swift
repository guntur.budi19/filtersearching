//
//  RoundImageView.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 04/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

@IBDesignable
class RoundImageView: UIImageView {
    @IBInspectable var cornerRadius:CGFloat = 20{
        didSet{
            setupView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    
    func setupView(){
        layer.cornerRadius = 20
//        layer.shadowColor = UIColor.darkText.cgColor
//        layer.shadowOpacity = 0.2
//        layer.shadowOffset = CGSize(width: 0.75, height: -0.75)
//        layer.shadowRadius = 4
        layer.masksToBounds = true
     
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
