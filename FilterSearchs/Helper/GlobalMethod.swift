//
//  GlobalMethod.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 04/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class GlobalMethod: NSObject {

    func setCollectionCellLayout(view: UICollectionView , numOfRow: Int) -> CGSize{
        
        let numberOfCellInRow = numOfRow
        var collectionCellWidth = CGFloat()
        var collectionCellHeight = CGFloat()
        
        var finalWidthWithPadding = CGFloat()
        var finalHeightWithPadding = CGFloat()
        
        
        collectionCellWidth = view.frame.size.width/CGFloat(numberOfCellInRow) - 1.0
        collectionCellHeight = collectionCellWidth
        finalWidthWithPadding = collectionCellWidth//-CGFloat(paddingBanner)
        finalHeightWithPadding = collectionCellHeight//-CGFloat(paddingBanner)
        
        return CGSize(width: finalWidthWithPadding, height: finalHeightWithPadding)
        
    }
    
    func formatBoundsImageView (view:UIImageView , setShadow:Bool) {
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        if(setShadow){
            self.setShadow(view: view)
        }
    }
    
    func setShadow(view:UIImageView){
        view.layer.shadowColor = UIColor.darkText.cgColor
        view.layer.shadowOpacity = 0.7
        view.layer.shadowOffset = CGSize(width: 1.5, height: -1)
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = true
        view.layer.shadowPath = UIBezierPath(rect: CGRect(x: view.bounds.origin.x+1, y: view.bounds.origin.y+2.0, width: view.bounds.width, height: view.bounds.height)).cgPath
    }
    
    
}
