//
//  RoundButtonView.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 04/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButtonView: UIButton {
    
    @IBInspectable var cornerRadius:CGFloat = 30{
        didSet{
            setupView()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    
    func setupView(){
        layer.cornerRadius = 20
        layer.shadowColor = UIColor.darkText.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1.35, height: -1.00)
        layer.shadowRadius = 5
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(rect: CGRect(x: self.bounds.origin.x+2, y: self.bounds.origin.y+5.0, width: self.bounds.width, height: self.bounds.height)).cgPath
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
