//
//  CollectionViewCell.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 04/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgResults: UIImageView!
    
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblItemPrice: UILabel!
    
    public var uriImage = String()
    var imageCache = NSCache<AnyObject, AnyObject>()
    
    override func awakeFromNib() {
        // self.contentView.translatesAutoresizingMaskIntoConstraints = false
        //var screenWidth = UIScreen.main.bounds.size.width
        super.awakeFromNib()
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    func fetchAsyncImageData(url : NSURL , imageView : UIImageView){
        let urlRequest = URLRequest(url: url as URL)
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = defaultSession.dataTask(with: urlRequest, completionHandler:  {(data, response, error) -> Void in
            if error != nil {
                print("error fetching image")
                return
            }
            
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                self.imageCache.setObject(imageToCache!, forKey: url)
                imageView.image = imageToCache
            }
        })
        
        dataTask.resume()
        
    }
    
    func setImage(){
        if let imageFromCache = imageCache.object(forKey: uriImage as AnyObject) as? UIImage
        {
            imgResults.image = imageFromCache
        }
        self.fetchAsyncImageData(url: URL(string: uriImage)! as NSURL, imageView: imgResults)
    }
    
}
