//
//  FilterViewController.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 04/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    
    var valMin = Int()
    var valMax = Int()
    var isWholeSale = Bool()
    var isGoldMerchant = Bool()
    var isOfficialStore = Bool()
    
    
    @IBOutlet var sliderView: UISlider!
    
    @IBOutlet var swWholeSale: UISwitch!
    @IBOutlet var swGoldMerchant: UISwitch!
    @IBOutlet var swOfficialStore: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valMin = 100
        valMax = 8000000
        isWholeSale = true
        isGoldMerchant = true
        isOfficialStore = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actSlide(_ sender: UISlider) {
        print(sender.value)
        if(sender.value == 0.0){
            valMax=100
        }else if (sender.value == 1.0){
            valMax = 8000000
        }else {
            valMax = Int(sender.value * 8000000)
        }
    }
   
    @IBAction func actSwWholeSale(_ sender: UISwitch) {
        if(isWholeSale){
            isWholeSale = false
        }else{
            isWholeSale = true
        }
    }
    
    @IBAction func actSwGoldMerchant(_ sender: UISwitch) {
        if(isGoldMerchant){
            isGoldMerchant = false
        }else{
            isGoldMerchant = true
        }
    }
    
    @IBAction func actSwOfficialStore(_ sender: UISwitch) {
        if(isOfficialStore){
            isOfficialStore = false
        }else{
            isOfficialStore = true
        }
    }
    
    @IBAction func actResetFilter(_ sender: Any) {
        sliderView.setValue(0.5, animated: true)
        swWholeSale.setOn(true, animated: true)
        swGoldMerchant.setOn(true, animated: true)
        swOfficialStore.setOn(true, animated: true)
        isOfficialStore = true
        isWholeSale = true
        isGoldMerchant = true
        valMin = 100
        valMax = 10000000
    }
    
    @IBAction func actApplyFilter(_ sender: Any) {

    }
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "presentFilter"{
            let navVC = segue.destination as? UINavigationController
            let vc = navVC?.viewControllers.first as! ViewController
            vc.isFiltered = true
            vc.isWholeSale = isWholeSale
            vc.isGoldMerchant = isGoldMerchant
            vc.isOfficialStore = isOfficialStore
            vc.pMin = valMax
            vc.pMax = 10000000
        }
    }
 

}
