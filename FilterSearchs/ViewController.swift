//
//  ViewController.swift
//  FilterSearchs
//
//  Created by Guntur Budi on 02/11/18.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class ViewController: UIViewController,URLSessionDelegate, URLSessionTaskDelegate  {

    
    
    @IBOutlet var collResults: UICollectionView!
    
    var uriTrans = String()
    var apiResults:NSArray = []
    var resultObject : [ResultObject]? = []
    var imageCache = NSCache<AnyObject, AnyObject>()
    var isGetData = Bool()
    var isReload = Bool()
    var page = Int()
    var range = Int()
    var totalPage = Int()
    var spinnerAtas = UIActivityIndicatorView()
    var spinnerBawah = UIActivityIndicatorView()
    var lastContentOffset = CGFloat()
    
    var isFiltered = Bool()
    var isWholeSale = Bool()
    var isGoldMerchant = Bool()
    var isOfficialStore = Bool()
    var pMin = Int()
    var pMax = Int()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        uriTrans = Urls().baseUrl
        
        if(isFiltered){
            self.checkFilter()
        }else{
            uriTrans = "https://ace.tokopedia.com/search/v2.5/product?q=samsung&pmin=10000&pmax=100000&wholesale=true&official=true&fshop=2"
            self.getData(uri: uriTrans)
        }
    }
    
    override func viewDidLoad() {
       // collResults.reloadData()
      //  self.getData()
        page = 1
        range = 10
        isGetData = false
        isReload = false
        self.formatSpinner()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func getData(uri:String){
        self.getData(uri: uri,page: page,range:range)
    }

    func formatSpinner(){
        spinnerAtas.style = .whiteLarge
        spinnerBawah.style = .whiteLarge
    }
    
    func checkFilter(){
        //&pmin=10000&pmax=100000&wholesale=true&official=true&fshop=2
        
        uriTrans = uriTrans + "&pmin=\(pMin)&pmax=\(pMax)"
        if(isWholeSale){
            uriTrans = uriTrans + "&wholesale=true"
        }else{
             uriTrans = uriTrans + "&wholesale=false"
        }
        
        if(isOfficialStore){
             uriTrans = uriTrans + "&official=true"
        }else{
            uriTrans = uriTrans + "&official=false"
        }
        
        if(isGoldMerchant){
            uriTrans = uriTrans + "&fshop=2"
        }else{
            uriTrans = uriTrans + "&fshop=3"
        }
        
        self.getData(uri:uriTrans)
        
    }
    
    func stopSpinner(){
        DispatchQueue.main.async {
            self.spinnerAtas.stopAnimating()
            self.spinnerAtas.removeFromSuperview()
            self.spinnerBawah.stopAnimating()
            self.spinnerBawah.removeFromSuperview()
        }
    }
    
    @IBAction func actFilter(_ sender: Any) {
    }
    
}

extension ViewController {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
        print(error.debugDescription)
    }
    
    func getData(uri: String, page: Int, range: Int) {
        //start=0&rows=10
        let url = URL(string: uri + "&start=\(page)&rows=\(range)")
        print("url : \(url!)")
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = "GET"
//        urlRequest.addValue(Fieldname().yoofixToken, forHTTPHeaderField: "yoofix-token")
        let boundary = "abcdefghij"
        let contentType = "application/x-www-form-urlencoded; boundary=\(boundary)"
        urlRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let body = NSMutableData()
        urlRequest.httpBody = body as Data
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = defaultSession.dataTask(with: urlRequest, completionHandler:  {(data, response, error) -> Void in
            if let data = data {
                do{
                    let jsonResponse = try JSONSerialization.jsonObject(with:
                        data, options: [])
                    guard let jsonArray = jsonResponse as? [String: Any] else {
                        return
                    }
                    guard let results = jsonArray["data"] as? NSArray else { return }
                    print("results \(results)")
                   // print("results count  : \(results.count)")
                    self.apiResults = results
                }catch let parsingErr{
                    print("error", parsingErr)
                }
                self.stopSpinner()
                if(self.isReload){
                    // print("masuk reload")
                    self.resultObject!.removeAll()
                    self.isReload = false
                }
               // self.isGetData=true
                self.setDataLoaded()
            }
        })
        
        dataTask.resume()
    }
    
   
    func setDataLoaded(){
//        DispatchQueue.main.async {
//            self.collResults.reloadData()
//        }
   
        for index in 0..<self.apiResults.count{
            var dict = NSDictionary()
            dict = self.apiResults[index] as! NSDictionary
            //print("dict : \(dict)")
            
            let _id = dict.object(forKey: "id") as! NSNumber
            let name = dict.object(forKey: "name") as! String
            let price = dict.object(forKey: "price") as! String
            
            let stock = dict.object(forKey: "stock") as! Int
            let urlImage = dict.object(forKey: "image_uri") as! String
            var image  = UIImage()
            let url:NSURL = NSURL(string: urlImage)!
           // let data:NSData = try! NSData(contentsOf: url as URL)
            let data = NSData(contentsOf: url as URL)
            image = UIImage(data: data! as Data) ?? UIImage(named: "unknown")!
            
            let urlObject = dict.object(forKey: "uri") as? String ?? ""
            let labels = dict.object(forKey: "labels") as? NSArray ?? []
            let shop = dict.object(forKey: "shop") as! NSDictionary
            let location = shop.object(forKey: "location") as! String
            
            let tempResults = ResultObject(_id: _id, name: name, price: price, location: location, stock: stock,uriImage: urlImage ,image: image ,  labels: labels, shop: shop, urlObj: urlObject)
            resultObject?.append(tempResults)
        }
        
        self.finishLoadingResults()
    }
    
    func finishLoadingResults(){
        DispatchQueue.main.async {
            self.collResults.reloadData()
        }
    }
    
    
    func fetchAsyncImageData(url : NSURL , imageView : UIImageView){
        let urlRequest = URLRequest(url: url as URL)
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
          let dataTask = defaultSession.dataTask(with: urlRequest, completionHandler:  {(data, response, error) -> Void in
            if error != nil {
                print("error fetching image")
                return
            }
            
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                self.imageCache.setObject(imageToCache!, forKey: url)
                imageView.image = imageToCache
            }
        })
        
        dataTask.resume()
        
    }
}



extension ViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resultObject!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let resultData = resultObject![indexPath.row]
        guard let url = URL(string: resultData.urlObj) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let idCell = "cellResults"
        
        var cell = CollectionViewCell()
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell,
                                                      for: indexPath) as! CollectionViewCell
        print("row : \(indexPath.row) : \(resultObject![indexPath.row])")
        let resultData = resultObject![indexPath.row]
        cell.imgResults.image = resultData.image
        cell.uriImage = resultData.uriImage
        cell.lblItemName.text = resultData.name
        cell.lblItemPrice.text = resultData.price
        //cell.setImage()
        
        if indexPath.row == resultObject!.count-1 {
            isGetData = true
        }
        
        return cell
        
    }
    
  
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        collectionView.setNeedsDisplay()
        return GlobalMethod().setCollectionCellLayout(view: collResults, numOfRow: 2)
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       // print("minimum spacing " )
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
}

extension ViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        
        
        // print("\(lastContentOffset)")
        
        if (lastContentOffset < -20 && isGetData && !isReload) {
            spinnerAtas.frame = CGRect(x: collResults.frame.size.width/2-20, y: (-60+lastContentOffset)/2, width: 45, height: 45)
            
            //tblDetailTransaction.addSubview(spinnerAtas)
            //spinnerAtas.startAnimating()
            collResults.addSubview(spinnerAtas)
            page = 0;
            range = 10;
            totalPage = 0
            isGetData = false
            self.apiResults = []
            // [self showLoadingAc:YES];
            isReload = true
            
            //self.isShowLoading(isShow: true)
           self.getData(uri:uriTrans)
            
            
        }
        
        if (scrollOffset + scrollViewHeight > scrollContentSizeHeight+20) {
            if (isGetData){//totalPagesAC) {
                spinnerBawah.frame = CGRect(x: collResults.frame.size.width/2-20, y: scrollContentSizeHeight, width: 45, height: 45)
                collResults.addSubview(spinnerBawah)
                spinnerBawah.startAnimating()
                page = page+1;
                isGetData = false
               self.getData(uri:uriTrans)
            }
            
        }
        //print(lastContentOffset)
        lastContentOffset = scrollView.contentOffset.y
    }
    
}

extension ViewController {
    public struct ResultObject{
      
        public var _id:NSNumber
        public var name: String
        public var price:String
        public var location: String
        public var stock: Int
        public var uriImage:String
        public var image:UIImage
        public var labels:NSArray
        public var shop:NSDictionary
        public var urlObj:String
        // public var picture: UIImage
    }
}
